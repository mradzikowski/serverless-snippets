# Serverless snippets

**Serverless plugin proposition**

## Idea

Create repository of reusable code snippets
for CloudFormation to be used in Serverless stacks.
Snippets should be possible to be placed in CF YAML
with a single-line call, replacing multi-line blocks of code
for repetitive definitions.

## Implementation

There are two possible implementations,
with native YAML anchors and with custom Serverless pre-processing.
They both have pros and cons.

In both cases it would act as a Serverless plugin,
new snippets could be added by the community
(by creating PR to the repo)
or by creating custom snippet in local repository to be used
for own needs.

## Option 1 - native YAML anchors

YAML supports
[anchors](https://medium.com/@kinghuang/docker-compose-anchors-aliases-extensions-a1e4105d70bd)
that could be used to define a structure once
and place it in multiple places.

In this case, plugin's role would be only to provide snippets
as anchors merged with the Serverless YAML file.
Then native YAML schema can be used to reference them.

Pros:

- using native YAML features, without introducing any new syntax
- easy to create new snippets - just in YAML

Cons:

- no option to pass a parameter to the snippet - snippets are static
- IDE would report anchors usage as invalid,
since the snippets would be defined in a file in the plugin repo

See example [serverless.yml](option1-anchors/serverless.yml) file.

## Option 2  - custom Serverless pre-processing

This options require introducing new special syntax
to reference snippets. Prior to generating CloudFormation
references would be replaced with the snippets content.

Pros:

- option to pass parameters to the snippets,
allowing to use them for much wider scope of actions

Cons:

- special syntax (proposition: `$$`)
- a little bit harder definition of new snippets (in JS code)

See example [serverless.yml](option2-custom/serverless.yml) file.
